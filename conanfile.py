from conans import ConanFile, CMake, tools
import os


class ScreenCaptureLiteConan(ConanFile):
    name = "screen_capture_lite"
    license = "MIT"
    description = "cross platform screen/window capturing library "
    homepage = "https://github.com/smasherprog/screen_capture_lite/"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-screen_capture_lite/"
    topics = ("screen-capture", "screen-ercorder", "cross-platform")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def source(self):
        # tools.get("https://github.com/smasherprog/screen_capture_lite/archive/{}.zip".format(self.version))
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("screen_capture_lite-{}".format(self.version), "screen_capture_lite")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="screen_capture_lite")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="screen_capture_lite/include")
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["screen_capture_lite_static", "X11", "Xinerama", "Xext", "Xfixes", "pthread"]
        elif self.settings.os == "Windows":
            self.cpp_info.libs = ["screen_capture_lite_static", ]
        elif self.settings.os == "Macos":
            self.cpp_info.libs = ["screen_capture_lite_static", "pthread"]
            for framework in ['Carbon',
                              'Cocoa',
                              'AudioToolbox',
                              'OpenGL',
                              'AVKit',
                              'AVFoundation',
                              'Foundation',
                              'IOKit',
                              'ApplicationServices',
                              'CoreText',
                              'CoreGraphics',
                              'CoreServices',
                              'CoreMedia',
                              'Security',
                              'ImageIO',
                              'System',
                              'WebKit']:
                self.cpp_info.exelinkflags.append('-framework %s' % framework)
            self.cpp_info.sharedlinkflags = self.cpp_info.exelinkflags
        else:
            self.cpp_info.libs = ["screen_capture_lite_static", ]
